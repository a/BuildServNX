# Exit on non-0 exit codes
set -e

curl https://gitlab.com/a/BuildServNX/raw/master/build_scripts/meta/libnx-master.sh | bash

sudo pacman -S zip unzip --noconfirm --needed # python python-pycryptodome --noconfirm --needed

SHORT_GIT=$(echo $GIT_COMMIT | cut -c 1-7)

# This is based on the Makefile inside atmosphere repo, the dist part
# However it also includes kips, exosphere and documentation

MAJORVER=$(grep '\ATMOSPHERE_RELEASE_VERSION_MAJOR\b' common/include/atmosphere/version.h | tr -s [:blank:] | cut -d' ' -f3)
MINORVER=$(grep '\ATMOSPHERE_RELEASE_VERSION_MINOR\b' common/include/atmosphere/version.h | tr -s [:blank:] | cut -d' ' -f3)
MICROVER=$(grep '\ATMOSPHERE_RELEASE_VERSION_MICRO\b' common/include/atmosphere/version.h | tr -s [:blank:] | cut -d' ' -f3)
AMSVER="$MAJORVER.$MINORVER.$MICROVER"

# HACKY :3
echo $AMSVER > bsnx/vername.txt

# Latest encrypted sept from AMS
wget https://elixi.re/i/1hj3yigv.enc -O /tmp/sept-secondary_00.enc
wget https://elixi.re/i/h9u28d76.enc -O /tmp/sept-secondary_01.enc

SEPT_00_ENC_PATH="/tmp/sept-secondary_00.enc" SEPT_01_ENC_PATH="/tmp/sept-secondary_01.enc" make dist

cd out
unzip ./*.zip
rm -f ./*.zip
cd ..

mv out/fusee-primary.bin bsnx/
mv out/ bsnx/sd/

cp -r docs/ bsnx/docs/
