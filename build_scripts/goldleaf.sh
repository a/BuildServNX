# Exit on non-0 exit codes
set -e

cd libnx
make
cd ..

cd Goldleaf
make
cd ..
cp -r Goldleaf/Output/* bsnx/
