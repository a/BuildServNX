# Exit on non-0 exit codes
set -e

sudo pacman -S python python-pip --noconfirm
mkdir sharkive/switch

cd switch
make
cd ..

cp switch/out/*.nro bsnx/
