# Exit on non-0 exit codes
set -e

curl https://gitlab.com/a/BuildServNX/raw/master/build_scripts/meta/libnx-pregfx.sh | bash

make
cp *.nro bsnx/
cp -r tools/ bsnx/tools/
