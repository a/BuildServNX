# Exit on non-0 exit codes
set -e

curl https://gitlab.com/a/BuildServNX/raw/master/build_scripts/meta/libnx-pregfx.sh | bash

curl https://cdn.discordapp.com/attachments/477945928196816935/525593866972168192/switch-ffmpeg-4.0.1-1-any.pkg.tar.xz > switch-ffmpeg-ihs.tar.xz
sudo pacman -U switch-ffmpeg-ihs.tar.xz --noconfirm

make

cp in-home-switching.* bsnx/
