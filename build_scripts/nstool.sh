# Exit on non-0 exit codes
set -e

sudo pacman -S cmake --noconfirm

mkdir build
cd build
cmake ..
make
cd ..

cp bin/Release/nstool bsnx/
