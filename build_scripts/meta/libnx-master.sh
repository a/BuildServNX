# Exit on non-0 exit codes
set -e

git clone https://github.com/switchbrew/libnx
cd libnx
make
sudo -E make install
