echo "bsnx5 pre-build script running :3"

cd /workspace/$JOB_BASE_NAME
mkdir bsnx
cp -r README* bsnx/
cp -r LICENSE* bsnx/

# Exit on non-0 exit codes
set -e

# Add doodledip repo
#sudo cat <<EOT >> /etc/pacman.conf
#[doodledip-aur]
#Server = https://doodledip.1a.pm/
#EOT

#sudo pacman-key --recv-key 538ED22367CEA8315E00F9FFF90EDD722F89275D
#sudo pacman-key --lsign-key 538ED22367CEA8315E00F9FFF90EDD722F89275D

# Force install a good version of switch-libconfig bc upstream broke signatures ugh
#sudo pacman -U https://doodledip.1a.pm/switch-libconfig-1.7.2-2-any.pkg.tar.xz --noconfirm

#sudo pacman -Sy
#sudo pacman -Syu --noconfirm --needed
