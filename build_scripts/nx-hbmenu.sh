# Exit on non-0 exit codes
set -e

curl https://gitlab.com/a/BuildServNX/raw/master/build_scripts/meta/libnx-master.sh | bash

sudo pacman -S zip --noconfirm

make nx

cp nx-hbmenu.* bsnx/
