# Exit on non-0 exit codes
set -e

make

mv sd_card bsnx/sd
mkdir -p bsnx/sd/atmosphere/titles/420000000000000E/flags
touch bsnx/sd/atmosphere/titles/420000000000000E/flags/boot2.flag
cp sys-ftpd.nsp bsnx/sd/atmosphere/titles/420000000000000E/exefs.nsp
