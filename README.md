# BuildServNX

```
   ____    ____     _   _      __  __   
U | __")u / __"| u | \ |"|     \ \/"/   
 \|  _ \/<\___ \/ <|  \| |>    /\  /\   
  | |_) | u___) | U| |\  |u   U /  \ u  
  |____/  |____/>> |_| \_|     /_/\_\   
 _|| \\_   )(  (__)||   \\,-.,-,>> \\_  
(__) (__) (__)     (_")  (_/  \_)  (__) 
```

BuildServNX (BSNX) is a Jenkins server configured to automatically build a large number of Nintendo Switch Console Modding related projects.

This repository includes the (sloppily written) build scripts and website assets related to it.

## Contributing

If you want to contribute to BSNX, create a bash file containing an install script for the project you want to see get added (see `build_scripts` folder for examples), create the header for your project in `website_assets`, and send a Merge Request (fancy Gitlab way of saying Pull Request).

In your Merge Request, please include details about:
- libnx requirements of the project (latest, master or a fork?)
- Git/SVN link of the project (currently, projects that aren't on git or svn won't be accepted)
- If the project takes a while to build, please state that as builds are automatically terminated at 15 minutes.

Proprietary projects that only have a repo containing binaries will not be accepted.

## Links

You can find BSNX' build download site at https://bsnx.lavatech.top/

You can find BSNX' Jenkins instance at https://jenkins.lavatech.top/

Builds are done with https://hub.docker.com/r/aveao/arch-jenkins-docker-jnlp-slave-dkp (repo [here](https://github.com/aveao/arch-jenkins-docker-jnlp-slave-dkp)), `build_scripts/meta/pre-build.sh` is ran before each build, `build_scripts/meta/post-build.sh` is ran after each build.

## License

The scripts and website assets are released under GPLv2.

The programs these scripts build may be released under different licenses.
